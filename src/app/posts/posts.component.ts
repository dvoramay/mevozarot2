import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class PostsComponent implements OnInit {
  
  posts;

  currentPost;

 select(post){
		this.currentPost = post; 
    console.log(	this.currentPost);
 }
  
  constructor(private _postsService: PostsService) {
this.posts = this._postsService.getPosts(); 
 }
  ngOnInit() //{ this.posts = this._postsService.getPosts();}
  

}