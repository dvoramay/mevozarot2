import { Injectable } from '@angular/core';

@Injectable()
export class PostsService {
  posts = [
   {author:'mike dean',title:'history of earth',content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget accumsan arcu. Mauris rhoncus malesuada ullamcorper. Nam pellentesque non nisi at tempor. Praesent ullamcorper tempor rhoncus. Aenean non venenatis mi. Ut commodo hendrerit viverra. Suspendisse turpis tellus, faucibus non magna eget, luctus cursus nisi. Morbi ipsum velit, vestibulum eu lectus accumsan, commodo lobortis ipsum. Nullam sodales mi eu sapien porta tincidunt. Praesent quis risus egestas, egestas velit id, porta urna. Morbi congue tellus nec mauris sodales, in aliquam nunc rhoncus.'},
   {author:'joakim noah',title:'basketball vs tennis',content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget accumsan arcu. Mauris rhoncus malesuada ullamcorper. Nam pellentesque non nisi at tempor. Praesent ullamcorper tempor rhoncus. Aenean non venenatis mi. Ut commodo hendrerit viverra. Suspendisse turpis tellus, faucibus non magna eget, luctus cursus nisi. Morbi ipsum velit, vestibulum eu lectus accumsan, commodo lobortis ipsum. Nullam sodales mi eu sapien porta tincidunt. Praesent quis risus egestas, egestas velit id, porta urna. Morbi congue tellus nec mauris sodales, in aliquam nunc rhoncus.'},
   {author:'mark z.',title:'The facebook',content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget accumsan arcu. Mauris rhoncus malesuada ullamcorper. Nam pellentesque non nisi at tempor. Praesent ullamcorper tempor rhoncus. Aenean non venenatis mi. Ut commodo hendrerit viverra. Suspendisse turpis tellus, faucibus non magna eget, luctus cursus nisi. Morbi ipsum velit, vestibulum eu lectus accumsan, commodo lobortis ipsum. Nullam sodales mi eu sapien porta tincidunt. Praesent quis risus egestas, egestas velit id, porta urna. Morbi congue tellus nec mauris sodales, in aliquam nunc rhoncus.'},
   {author:'BB king',title:'sultan of the blues',content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget accumsan arcu. Mauris rhoncus malesuada ullamcorper. Nam pellentesque non nisi at tempor. Praesent ullamcorper tempor rhoncus. Aenean non venenatis mi. Ut commodo hendrerit viverra. Suspendisse turpis tellus, faucibus non magna eget, luctus cursus nisi. Morbi ipsum velit, vestibulum eu lectus accumsan, commodo lobortis ipsum. Nullam sodales mi eu sapien porta tincidunt. Praesent quis risus egestas, egestas velit id, porta urna. Morbi congue tellus nec mauris sodales, in aliquam nunc rhoncus.'}

]
  getPosts(){
		return this.posts;
	}

  constructor() { }

}